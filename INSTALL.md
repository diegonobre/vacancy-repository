# Installation guide for Vacancy Repository API

## Requirements
```sh
# as sudo...
apt-get update
apt-get upgrade -y

# apache
apt-get install python-software-properties -y
apt-get install apache2 libapache2-mod-php7.0 libapache2-mod-python libapache2-mod-python-doc libapache2-mod-wsgi -y
ufw allow in "Apache Full"
rm /etc/apache2/mods-enabled/python.load
# enabling apache modules
a2enmod rewrite
service apache2 restart

# php modules
apt-get install php7.0 php7.0-cli php7.0-dev php7.0-pgsql php7.0-mbstring php-xdebug php7.0-pgsql php7.0-mysql php7.0-gd php-pear php7.0-curl php7.0-intl php-pear php7.0-fpm -y

# mysql database
apt-get install mysql-server-5.7 -y
apt-get install mysql-client-5.7 -y

# utils
apt-get install git git-flow composer redis-server zip rpl nodejs npm python httpie -y
apt-get install vim nmap axel htop -y

# npm + gulp
npm install -g gulp
npm config set registry http://registry.npmjs.org/

# redis installation from source (if choose not use apt-get install redis-server)
cd /tmp
wget https://github.com/phpredis/phpredis/archive/php7.zip -O phpredis.zip
unzip -o /tmp/phpredis.zip && mv /tmp/phpredis-* /tmp/phpredis && cd /tmp/phpredis && phpize && ./configure && make && sudo make install
sudo touch /etc/php/7.0/mods-available/redis.ini && echo extension=redis.so > /etc/php/7.0/mods-available/redis.ini
sudo ln -s /etc/php/7.0/mods-available/redis.ini /etc/php/7.0/apache2/conf.d/redis.ini
sudo ln -s /etc/php/7.0/mods-available/redis.ini /etc/php/7.0/fpm/conf.d/redis.ini
sudo ln -s /etc/php/7.0/mods-available/redis.ini /etc/php/7.0/cli/conf.d/redis.ini
sudo service php7.0-fpm restart
sudo service apache2 restart

# phpunit
wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit

# java
add-apt-repository ppa:webupd8team/java -y
apt-get update
apt-get install oracle-java7-installer
```

### New Symfony 3.x project
```sh
git clone git@gitlab.com:diegonobre/vacancy-repository.git
cd vacancy-repository
composer install
```

### Setting up database
```sql
mysql -u root -p
# create database
CREATE DATABASE vacancy_repository;
# create user
CREATE USER 'usr_vacancy_repository'@'localhost' IDENTIFIED BY '#VacancyRepository';
# set privileges
GRANT ALL PRIVILEGES ON vacancy_repository.* TO 'usr_vacancy_repository'@'localhost' IDENTIFIED BY '#VacancyRepository';
flush privileges;
```

### Generating doctrine entities
```sh
php bin/console doctrine:schema:update --force
```

### Populate ElasticSearch
```sh
php bin/console fos:elastica:populate
```
