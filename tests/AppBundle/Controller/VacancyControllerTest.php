<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VacancyControllerTest extends WebTestCase
{
    public function testCompleteScenario()
    {
        // Create a new entry in the database
        $client = static::createClient();

        $crawler = $client->request('GET', '/vacancy/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form(array(
            'vacancy[title]'  => 'Title Test',
            'vacancy[content]'  => 'Content Test',
            'vacancy[description]'  => 'Description Test',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0,
            $crawler->filter('td:contains("Title Test")')->count(),
            'Missing element td:contains("Title Test")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Edit')->link());

        $form = $crawler->selectButton('Update')->form(array(
            'vacancy[title]'  => 'Title Test Updated',
            'vacancy[content]'  => 'Content Test Updated',
            'vacancy[description]'  => 'Description Test Updated',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        $this->assertGreaterThan(0,
            $crawler->filter('[value="Title Test Updated"]')->count(),
            'Missing element [value="Title Test Updated"]');

        // Delete the entity
        $client->submit($crawler->selectButton('Delete')->form());

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/Title Test Updated/', $client->getResponse()->getContent());
    }
}
