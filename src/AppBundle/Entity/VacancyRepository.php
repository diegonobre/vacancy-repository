<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;

class VacancyRepository extends EntityRepository
{
    public function save(Vacancy $vacancy, $elasticPersister = null)
    {
        $em = $this->getEntityManager();
        $em->persist($vacancy);
        $em->flush();

        // TODO forcing while I can't debug why listeners doesn't work
        if (isset($elasticPersister))
            $elasticPersister->replaceOne($vacancy);

        $this->clearCache('listLatest');

        return $vacancy;
    }

    public function delete(Vacancy $vacancy)
    {
        $em = $this->getEntityManager();
        $em->remove($vacancy);
        $em->flush();

        $this->clearCache('listLatest');
    }

    public function listLatest($limit = 10)
    {
        return $this->getEntityManager()
            ->createQuery("SELECT v FROM AppBundle\Entity\Vacancy v ORDER BY v.id DESC")
            ->setMaxResults($limit)
            ->useResultCache(true, 3600, 'listLatest')
            ->getResult();
    }

    private function clearCache($cacheId)
    {
        $this->getEntityManager()
            ->getConfiguration()
            ->getResultCacheImpl()
            ->delete($cacheId);
    }
}
