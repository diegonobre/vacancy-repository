<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Vacancy;
use AppBundle\Form\Type\VacancyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class VacancyController
 *
 * @package AppBundle\Controller
 * @Route("/vacancy")
 */
class VacancyController extends Controller
{
    /**
     * Displays a form to create a new Vacancy entity.
     *
     * @Route("/new", name="vacancy_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new Vacancy();
        $form   = $this->createCreateForm($entity);

        return $this->render('vacancy/create.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Vacancy entity
     *
     * @Route("/", name="vacancy_create")
     * @Method("POST")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $entity = new Vacancy();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $this->getDoctrine()
                ->getRepository('AppBundle:Vacancy')
                ->save($entity);

            return $this->redirect($this->generateUrl('vacancy_show', array('id' => $entity->getId())));
        }

        return $this->render('vacancy/create.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Vacancy entity.
     *
     * @Route("/{id}/edit", name="vacancy_edit")
     * @Method("GET")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Vacancy')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Vacancy entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('vacancy/create.html.twig', array(
            'entity' => $entity,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Vacancy entity.
     *
     * @Route("/{id}", name="vacancy_update")
     * @Method("PUT")
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Vacancy')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Vacancy entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $elasticPersister = $this->get('fos_elastica.object_persister.app.vacancy');

            $this->getDoctrine()
                ->getRepository('AppBundle:Vacancy')
                ->save($entity, $elasticPersister);

            return $this->redirect($this->generateUrl('vacancy_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Vacancy entity.
     *
     * @Route("/{id}", name="vacancy_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Vacancy')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Vacancy entity.');
            }

            $this->getDoctrine()
                ->getRepository('AppBundle:Vacancy')
                ->delete($entity);
        }

        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * Finds and displays a Vacancy entity.
     *
     * @Route("/{id}", name="vacancy_show")
     * @Method("GET")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Vacancy')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Vacancy entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('vacancy/show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to create a Vacancy entity.
     *
     * @param Vacancy $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Vacancy $entity)
    {
        $form = $this->createForm(VacancyType::class, $entity, array(
            'action' => $this->generateUrl('vacancy_create'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Creates a form to edit a Vacancy entity.
     *
     * @param Vacancy $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Vacancy $entity)
    {
        $form = $this->createForm(VacancyType::class, $entity, array(
            'action' => $this->generateUrl('vacancy_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }

    /**
     * Creates a form to delete a Vacancy entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('vacancy_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
            ;
    }
}
