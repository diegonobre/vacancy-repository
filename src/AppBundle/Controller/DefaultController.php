<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $vacancyList = $this->getDoctrine()
            ->getRepository('AppBundle:Vacancy')
            ->listLatest();

        return $this->render('default/index.html.twig', array(
            'vacancyList' => $vacancyList,
        ));
    }
}
